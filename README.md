## riva-user 7.1.2 N2G47H V10.1.1.0.NCKMIFI release-keys
- Manufacturer: xiaomi
- Platform: msm8937
- Codename: rova
- Brand: Xiaomi
- Flavor: cherish_rova-userdebug
- Release Version: 12
- Id: SP1A.210812.016
- Incremental: eng.build.20211029.033507
- Tags: test-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: Xiaomi/riva/riva:7.1.2/N2G47H/V10.1.1.0.NCKMIFI:user/release-keys
- OTA version: 
- Branch: riva-user-7.1.2-N2G47H-V10.1.1.0.NCKMIFI-release-keys
- Repo: xiaomi_rova_dump_17444


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
